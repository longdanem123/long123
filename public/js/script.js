
(function(window){
}
    var ajaxList = new ajaxListView('#ajax-list-view');
})(window);

function ajaxListView(obj) {
    var obj = $(obj);
    if(obj.length == 0) return false;
    var table = $('#table-view-data');
    var notFound = table.find('.data-not-found');
    table = table.find('tbody');
    var default_row = table.find('.clone-data');
    table.find('.clone-data').remove();

    var methods = {
        init: function() {},
        load: function() {},
        clearData : function () {},
        renderElement : function () {},
        renderData : function () {},
        loadPage : function () {},
        handleEvents: function () {},
    };
    var config = {
        ajax_url : obj.attr('ajax_url'),
        list_type :obj.attr('list_type'),
        status : 'all',
        orderBy: 'ASC',
        employee_id:'',
    }

    var status = {
        all : '',
        publish: '',
        invisible: '',
        trash:'',
    }
    methods.init = function() {
        methods.switchData();
        methods.load();
        methods.filterHandleEvents();
        methods.handleSearch();
        methods.handleSort();
        methods.handleSelectedForFunction();
        methods.handleSelectedForJob();
        methods.handleSelectedForSkill();
    };
    methods.switchData = function () {
        switch (config.list_type) {
            case 'employee':{
                break;
            }
            default: break;
        }
    }
    methods.setStatus = function(source, key){
        return (source.hasOwnProperty(key) && ! isNaN(source[key])) ? source[key] : 0;
    }
    methods.updateCount = function() {
        $('.btn_count_status').each(function(index, el) {
            var state = $(this).attr('data-status');
            switch (state) {
                case 'all':{
                    $(this).find('.count_value').html(status.all);
                    break;
                }
                case 'publish':{
                    $(this).find('.count_value').html(status.publish);
                    break;
                }
                case 'invisible':{
                    $(this).find('.count_value').html(status.invisible);
                    break;
                }
                case 'trash':{
                    $(this).find('.count_value').html(status.trash);
                    break;
                }
            }
        });
    };
    methods.updateStatus = function (array) {
        status.all = methods.setStatus(array,'all');
        status.publish = methods.setStatus(array,'publish');
        status.invisible = methods.setStatus(array,'invisible');
        status.trash = methods.setStatus(array,'trash');
    }
    methods.load = function() {
        var sendData = new FormData();
        console.log(config);
     //   sendData.append('paged', config.paged);
      //  sendData.append('keyword',config.keyword);
      //  sendData.append('limit',config.limit);
        sendData.append('status',config.status);
       // sendData.append('order_by',config.orderBy);
        switch (config.list_type) {
            case 'employee':{
                break;
            }
            default: break;
        }
        $.ajax({
            url: config.ajax_url,
            type: 'POST',
            data: sendData,
            contentType: false,
            processData: false,
            timeout: 5000,
            beforeSend: function () {
            },
            error: function (x, t, m) {

            },
            success: function (getData) {
                console.log(getData);
                if(getData.success == true){
                    if(getData.data.current_page == 1 && getData.data.data.length == 0){
                        pagi.twbsPagination('destroy');
                        methods.clearData();
                        notFound.removeClass('hide');
                        methods.updateStatus(getData.data.status);
                        methods.updateCount();
                    }else if(getData.data.current_page > 1 && getData.data.data.length == 0 ){
                        config.paged = getData.data.current_page - 1;
                        methods.load();
                    }else{
                        var totalPage = getData.data.num_page;
                        _data = getData.data.data;
                        notFound.addClass('hide');
                        methods.clearData();
                        methods.updateStatus(getData.data.status);
                        methods.updateCount();
                        if(_data.length){
                            pagi.twbsPagination('destroy');
                            methods.renderData(_data);
                            if(totalPage > 1){
                                methods.initPagi(totalPage);
                            }
                        }
                    }
                }
            }
        });
    };
    methods.clearData = function () {
        table.html('');
    }
    methods.loadPage = function (page){
        var sendData = new FormData();
        console.log(config);
        config.paged = page;
        sendData.append('paged', config.paged);
        sendData.append('keyword',config.keyword);
        sendData.append('limit',config.limit);
        sendData.append('status',config.status);
        sendData.append('order_by',config.orderBy);
        switch (config.list_type) {
            case 'industries':{
                break;
            }
            case 'functions':{
                sendData.append('industry_id',config.industry_id);
                break;
            }
            case 'jobs':{
                sendData.append('industry_id',config.industry_id);
                sendData.append('function_id',config.function_id);
                break;
            }
            case 'skills':{
                sendData.append('industry_id',config.industry_id);
                sendData.append('function_id',config.function_id);
                sendData.append('job_id',config.job_id);
                break;
            }
            default: break;
        }

        $.ajax({
            url: config.ajax_url,
            type: 'POST',
            data: sendData,
            contentType: false,
            processData: false,
            timeout: 5000,
            beforeSend: function () {
            },
            error: function (x, t, m) {

            },
            success: function (getData) {
                console.log(getData);
                if(getData.success === true){
                    methods.clearData();
                    _data = getData.data.data;
                    methods.updateStatus(getData.data.status);
                    methods.renderData(_data);
                }
            }
        });
    }
    methods.initPagi = function (total) {
        pagi.twbsPagination({
            'totalPages': total,
            'startPage':config.paged,
            'visiblePages': 5,
            'last':null,
            'first':null,
            'next': '<span class="fa fa-caret-right"></span>',
            'prev': '<span class="fa fa-caret-left"></span>',
            'hideOnlyOnePage':true,
            'initiateStartPageClick': false,
            onPageClick: function (event, page) {
                methods.loadPage(page);
            }
        });
    }
    methods.renderData = function (array) {
        var index = config.paged * config.limit - config.limit + 1 ;
        methods.clearData();
        for (var i = 0; i < array.length; i++) {
            methods.renderElement(array[i],index++);
        }
        methods.handleEvents();
    }
    methods.renderElement = function (obj,index) {
        var row = default_row.clone();
        row.removeClass('clone-data');
        row.attr('id',obj._id);
        row.attr('status',obj.status);
        row.find('.td-index').html(index);
        switch (config.list_type) {
            case 'employee':{
                row.find('.td-name').html(obj.name);
                break;
            }
            default: break;

        }
        var list = row.find('.btn-action');
        list.each(function(index, el) {
            var href = $(this).attr('href');
            href = href +'/'+obj._id;
            $(this).attr('href',href);
        });
        switch (config.status) {
            case 'all':{
                row.find('.btn_action_delete').remove();
                if(obj.status == 'publish'){
                    row.find('.btn_action_publish').addClass('hide');
                }else if(obj.status == 'invisible'){
                    row.find('.btn_action_invisible').addClass('hide');
                }
                break;
            }
            case 'publish':{
                row.find('.btn_action_delete').remove();
                row.find('.btn_action_publish').remove();
                break;
            }
            case 'invisible':{
                row.find('.btn_action_delete').remove();
                row.find('.btn_action_invisible').remove();
                break;
            }
            case 'trash':{
                row.find('.btn_action_invisible').remove();
                row.find('.btn_action_translate').remove();
                row.find('.btn_action_edit').remove();
                row.find('.btn_action_trash').remove();
                break;
            }
            default:
        }

        row.appendTo(table).show();
        methods.handleEvents();
        return ;
    }
    methods.handleEvents = function () {
        $('.btn_action_publish').unbind('click').bind('click', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            var parent = $(this).parents('tr');
            $.ajax({
                url: href,
                type: 'GET',
                contentType: false,
                processData: false,
                timeout: 5000,
                beforeSend: function () {
                },
                error: function (x, t, m) {

                },
                success: function (getData) {
                    console.log(getData);
                    if(getData.success == true){
                        switch (config.status) {
                            case 'all':{
                                parent.find('.btn_action_publish').addClass('hide');
                                parent.find('.btn_action_invisible').removeClass('hide');
                                methods.updateStatus(getData.status);
                                methods.updateCount();
                                break;
                            }
                            case 'publish':{
                                break;
                            }
                            case 'invisible':{
                                methods.load();

                                break;
                            }
                            case 'trash':{
                                methods.load();
                                // console.log(status);
                                break;
                            }
                            default:
                        }
                    }
                }
            });

        })
        $('.btn_action_invisible').unbind('click').bind('click', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            var parent = $(this).parents('tr');
            $.ajax({
                url: href,
                type: 'GET',
                contentType: false,
                processData: false,
                timeout: 5000,
                beforeSend: function () {
                },
                error: function (x, t, m) {

                },
                success: function (getData) {
                    console.log(getData);
                    if(getData.success == true){
                        switch (config.status) {
                            case 'all':{
                                parent.find('.btn_action_invisible').addClass('hide');
                                parent.find('.btn_action_publish').removeClass('hide');
                                methods.updateStatus(getData.status);
                                methods.updateCount();
                                break;
                            }
                            case 'publish':{
                                methods.load();
                                break;
                            }
                            case 'invisible':{
                                break;
                            }
                            case 'trash':{
                                break;
                            }
                            default:
                        }
                    }
                }
            });

        })
        $('.btn_action_delete').unbind('click').bind('click', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            var parent = $(this).parents('tr');
            $.confirm({
                title: config.deleteAlert.title,
                content: config.deleteAlert.content,
                type: 'red',
                typeAnimated: true,
                animation: 'zoom',
                closeAnimation: 'scale',
                closeIcon: true,
                buttons: {
                    close: {
                        text: config.deleteAlert.cancel,
                        btnClass: 'btn btn-danger'
                    },
                    ok: {
                        text: config.deleteAlert.done,
                        btnClass: 'btn btn-danger',
                        action: function () {
                            $.ajax({
                                url: href,
                                type: 'GET',
                                contentType: false,
                                processData: false,
                                timeout: 5000,
                                beforeSend: function () {
                                },
                                error: function (x, t, m) {

                                },
                                success: function (getData) {
                                    // console.log(getData);
                                    if (getData.success == true) {
                                        methods.load();
                                    }
                                }
                            });
                        }
                    }
                }
            });
        $('.btn_action_trash').unbind('click').bind('click', function (event) {
            event.preventDefault();
            var href = $(this).attr('href');
            var parent = $(this).parents('tr');
            $.ajax({
                url: href,
                type: 'GET',
                contentType: false,
                processData: false,
                timeout: 5000,
                beforeSend: function () {
                },
                error: function (x, t, m) {

                },
                success: function (getData) {
                    if(getData.success == true){
                        methods.load();
                    }
                }
            });

        })

    }
    methods.filterHandleEvents = function () {
        $('.btn_count_status').click(function(event) {
            event.preventDefault();
            config.status = $(this).attr('data-status');
            config.paged = 1;
            pagi.twbsPagination('destroy');
            methods.load();
            $('.btn_count_status').not($(this)).removeClass('active');
            $(this).addClass('active');
        });
    }
    methods.handleSearch = function () {
        $('#search-box').keyup(function(event) {
            event.preventDefault();
            var keyword = $(this).val();
            keyword = keyword.trim();
            config.keyword = keyword;
            if( config.keyword == '' && $(this).attr('default-value') == '' ){
                return;
            }
            $(this).attr('default-value', config.keyword);
            if(keyword == ''){
                methods.load();
            }else{
                var sendData = new FormData();
                config.paged = 1;
                config.keyword = keyword;
                sendData.append('paged', config.paged);
                sendData.append('keyword',config.keyword);
                sendData.append('limit',config.limit);
                sendData.append('status',config.status);
                sendData.append('order_by',config.orderBy);
                switch (config.list_type) {
                    case 'industries':{
                        break;
                    }
                    case 'functions':{
                        sendData.append('industry_id',config.industry_id);
                        break;
                    }
                    case 'jobs':{
                        sendData.append('industry_id',config.industry_id);
                        sendData.append('function_id',config.function_id);
                        break;
                    }
                    case 'skills':{
                        sendData.append('industry_id',config.industry_id);
                        sendData.append('function_id',config.function_id);
                        sendData.append('job_id',config.job_id);
                        break;
                    }
                    default: break;
                }
                console.log(config);
                $.ajax({
                    type: 'GET',
                    url: config.ajax_url,
                    type: 'POST',
                    data: sendData,
                    contentType: false,
                    processData: false,
                    timeout: 5000,
                    beforeSend: function () {
                    },
                    error: function (x, t, m) {
                    },
                    success: function (getData) {
                        // console.log(getData);
                        if(getData.success == true){
                            var totalPage = getData.data.num_page;
                            _data = getData.data.data;
                            methods.clearData();
                            pagi.twbsPagination('destroy');
                            if(_data.length){
                                methods.renderData(_data);
                                if(totalPage > 1){
                                    methods.initPagi(totalPage);
                                }
                            }
                        }
                    }
                });
            }
        });

    }
    methods.handleSort = function () {
        $('.icon_sort').click(function(event) {
            event.preventDefault();
            config.paged = 1;
            var current = $(this).attr('data-sort');
            if(current == 'ASC'){
                $(this).attr('data-sort','DESC');
                config.orderBy = 'DESC';
            }else{
                $(this).attr('data-sort','ASC');
                config.orderBy = 'ASC';
            }
            methods.load();
        });
    }
    methods.handleSelectedForFunction = function () {
        $('.oneSelect').change(function(event) {
            event.preventDefault();
            config.industry_id = $(this).val();
            pagi.twbsPagination('destroy');
            methods.load();
        });
    }
    methods.handleSelectedForJob = function () {
        $('.twoSelect').change(function(event) {
            event.preventDefault();
            var $this = $(this);
            var target = $(this).attr('data-target');
            var name = $(this).attr('name');
            var value = $(this).find('option:selected').val();
            if(target !== undefined){
                var ajax_url = $(this).attr('get_url');
                if (ajax_url !== undefined) {
                    ajax_url = ajax_url+'/'+ value;
                    console.log(ajax_url);
                    $.ajax({
                        url: ajax_url,
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            $(target).html('');
                            var default_option = $(target).attr('data-default');
                            if(default_option === undefined){
                                default_option = 'All Functions';
                            }
                            var html = '<option value="all" selected>'+default_option+'</option>'
                            $.each(data , function(index, element) {
                                html += '<option value="'+element._id+'">'+element.name+'</option>'
                            });
                            $(target).append(html);
                        }
                    });
                }
            }
            switch (name) {
                case 'industries':{
                    config.industry_id = $(this).val();
                    config.function_id = 'all';
                    break;
                }
                case 'functions':{
                    config.function_id = $(this).val();
                    break;
                }
                default:{
                    break;
                }
            }
            methods.load();
        });
    }
    methods.handleSelectedForSkill = function () {
        var industriesSelect = $('.threeSelect[name=industries]');
        var functionsSelect = $('.threeSelect[name=functions]');
        var jobsSelect = $('.threeSelect[name=jobs]');
        var functionsSelect_default = functionsSelect.attr('data-default');
        if(functionsSelect_default === undefined) functionsSelect_default = 'All Functions';
        var jobsSelect_default = jobsSelect.attr('data-default');
        if(jobsSelect_default === undefined) jobsSelect_default = 'All Functions';

        $('.threeSelect').change(function(event) {
            event.preventDefault();
            var name = $(this).attr('name');
            var value = $(this).val();
            switch (name) {
                case 'industries':{
                    config.industry_id = value;
                    config.function_id = 'all';
                    config.job_id = 'all';
                    var ajax_url = $(this).attr('get_url');
                    if (ajax_url !== undefined) {
                        ajax_url = ajax_url+'/'+ value;
                        $.ajax({
                            url: ajax_url,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                console.log(data);
                                if(data.success == true){
                                    functionsSelect.html('');
                                    jobsSelect.html('');

                                    var html_functions = '<option value="all" selected>'+functionsSelect_default+'</option>';
                                    $.each(data.data.functions , function(index, element) {
                                        html_functions += '<option value="'+element._id+'">'+element.name+'</option>'
                                    });
                                    functionsSelect.append(html_functions);

                                    var html_jobs = '<option value="all" selected>'+jobsSelect_default+'</option>';
                                    $.each(data.data.jobs , function(index, element) {
                                        html_jobs += '<option value="'+element._id+'">'+element.name+'</option>'
                                    });
                                    jobsSelect.append(html_jobs);
                                }
                            }
                        });
                    }
                    break;
                }
                case 'functions':{
                    config.function_id = value;
                    config.job_id = 'all';
                    var ajax_url = $(this).attr('get_url');
                    if (ajax_url !== undefined) {
                        ajax_url = ajax_url+'/'+ value;
                        $.ajax({
                            url: ajax_url,
                            type: "GET",
                            dataType: "json",
                            success: function (data) {
                                console.log(data);
                                if(data.success == true){
                                    jobsSelect.html('');
                                    var html = '<option value="all" selected>'+jobsSelect_default+'</option>'
                                    if(data.data !== null){
                                        $.each(data.data , function(index, element) {
                                            html += '<option value="'+element._id+'">'+element.name+'</option>'
                                        });
                                    }
                                    jobsSelect.append(html);
                                }
                            }
                        });
                    }

                    break;
                }
                case 'jobs':{
                    config.job_id = value;
                    break;
                }
                default: break;

            }
            methods.load();
        });
    }
    methods.init();
    return this;
}