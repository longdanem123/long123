﻿function Staff_project() {
    var _list_type=this.list_type;
    var _saveEmpl = this.saveEmpl = function saveEmpl(element) {
        var tr = $(element).closest('tr');
        switch (_list_type) {

            case 'staff':
            {
                var id = $(tr).attr('id');
                var obj = {};

                var child = tr.find('input');
                $(child).each(function () {
                    obj[$(this).attr('name')] = $(this).val();

                })
                obj['staff_id'] = id;
                _ajaxEditProjectStaff(obj);
                //ajax edit here

                break;
            }
            case 'work':
            {
                var id = $(tr).attr('work_id');
                //ajax edit here
                break;
            }
            default: break;
        }
        var child = tr.find('input');
        $(child).each(function () {
            $(this).attr('default_value', $(this).val());
            $(this).attr('disabled', true)
        })
        var ctd = tr.find('td');
        _addButtonEditDelete(ctd[ctd.length-1]);


    };
    var _cancleEmpl = this.cancleEmpl = function cancelEmpl(element) {
        var tr = $(element).closest('tr');
        var child = tr.find('input');
        $(child).each(function () {
            var d = $(this).attr('default_value');
            if (d !== undefined) {
                $(this).val(d);
            }
            $(this).attr('disabled', true);
        })
        var ctd = tr.find('td');
        _addButtonEditDelete(ctd[ctd.length-1]);

    };
    var _editEmpl = this.editEmpl = function editEmpl(element) {
        var tr = $(element).closest('tr');
        var child = tr.find('input');
        $(child).each(function () {
            $(this).attr('default_value', $(this).val());
            $(this).attr('disabled', false);
        })
        switch (_list_type)
        {

            case 'staff':{
                if ( tr.attr('isout')=='0' ) {
                    $(child[0]).attr('disabled', "disabled");
                    $(child[1]).attr('disabled', "disabled");
                    $(child[2]).attr('disabled', "disabled");
                }
                break;
            }

            case 'work':{
                $(child[5]).attr('disabled', "disabled");
                $(child[6]).attr('disabled', "disabled");
                break;
            }
            default: break;
        }
        var ctd = tr.find('td');
        _addButtonSaveCancle(ctd[ctd.length-1]);


    };
    var _deleteEmpl = this.deleteEmpl = function deleteEmpl(element) {

        var tr = $(element).closest('tr');
        switch (_list_type) {

            case 'staff':
            {
                var id = $(tr).attr('id');
                //ajax delete here
                _ajaxDelete(id);
                var child = tr.find('input');
                var newop = '<option value="' + id + '" opemail="' + $(child[1]).val() + '" opphone="' + $(child[2]).val() + '" isout="' + $(tr).attr('isout') + '">' + $(child[0]).attr('value')+ '</option>';
                $('#addName').prepend(newop);
                $('#addName').trigger("chosen:updated");
                $('#addName').trigger("change");
                break;
            }
            case 'work':
            {
                var id = $(tr).attr('work_id');
                //ajax delete here
                var child = tr.find('input');
                var returnManager = '<option value="' + $(child[5]).attr("id") + '" >' + $(child[5]).attr("value") + '</option>';
                $('#manager').prepend(returnManager);
                if($(child[6]).attr("value")) {
                    var arrval = $(child[6]).attr("value").split(',');
                    var arrid = $(child[6]).attr("id").split(',');
                    for (var i = 0; i < arrval.length; i++) {
                        var rManager = '<option value="' + arrid[i] + '" >' + arrval[i] + '</option>';
                        $('#manager').prepend(rManager);
                    }
                }
                $('#manager').trigger("chosen:updated");
                $('#manager').trigger("change");
                break;
            }
            default: break;
        }

        tr.remove();
    };
    var _addExEmpl = this.addExEmpl = function addExEmpl() {
        if ($("#form-addNewEmployee").valid()) {
            var obj = {};
            obj['name'] = $("#name").val();
            obj['phone'] = $("#phone").val();
            obj['email'] = $("#email").val();
            var id = _ajaxAddExEmpl(obj);
            var newop = '<option value="' + id + '" opemail="' + $('#email').val() + '" opphone="' + $('#phone').val() + '" isout="1">' + $("#name").val() + '</option>';
            $('#addName').prepend(newop);
            $('#addName').trigger("chosen:updated");
            $('#form-addNewEmployee').trigger("chosen:updated");
            $("#name").val('');
            $('#email').val('');
            $('#phone').val('');
            $('#addName').trigger("change");
            $(".group-create").toggleClass("open");
        }
    };
    var _addButtonSaveCancle=this.addButtonSaveCancle=function addButtonSaveCancle(element){
        $(element).html('<button type="button" class="save-emp" ><i class="fa fa-save"></i></button><button type="button" class="cancle-emp" ><i class="fa fa-ban"></i></button>');
        $(".cancle-emp").click(function (e) {
            e.preventDefault();
            if ($(this).attr('cancle-event') === undefined) {
                _cancleEmpl(this);
                $(this).attr('cancle-event', true);
            }

        });
        $(".save-emp").click(function (e) {
            e.preventDefault();
            if ($(this).attr('save-event') === undefined) {
                _saveEmpl(this);
                $(this).attr('save-event', true);
            }

        });
    };
    var _addButtonEditDelete=this.addButtonEditDelete=function addButtonEditDelete(element){
        $(element).html('<button type="button" class="edit-emp" ><i class="fa fa-pencil"></i></button><button type="button" class="delete-emp" ><i class="fa fa-trash-o"></i></button>');
        $(".edit-emp").click(function (e) {
            e.preventDefault();
            if ($(this).attr('edit-event') === undefined) {
                _editEmpl(this);
                $(this).attr('edit-event', true);
            }

        });
        $(".delete-emp").click(function (e) {
            e.preventDefault();
            if ($(this).attr('delete-event') === undefined) {
                _deleteEmpl(this);
                $(this).attr('delete-event', true);
            }

        });

    };
    var _setValueForClone=this.setValueForClone=function setValueForClone(element,tr_id) {
        var c_input = $(element).find("input");
        for (var i = 0; i < c_input.length; i++) {
            var _name = $(c_input[i]).attr("name");
            var test = $(tr_id).find('input[name=' + _name + ']');
            var value = $(test).val();
            $(c_input[i]).val(value);
            $(c_input[i]).attr("disabled", "disabled");
            $(test).val('');
        }

        var c_select = $(element).find("select");
        for (var j = 0; j < c_select.length; j++) {
            if( $(c_select[j]).attr("multiple")===undefined) {
                var _name = $(c_select[j]).attr("name");
                var sel = $(tr_id).find('select[name=' + _name + ']');
                var op = $(sel).find("option:selected");
                var text = $(op).text();
                var value = $(op).val();
                var newinput = '<input name="' + _name + '" class="' + $(sel).attr('class') + '" type="text"  disabled="true" value="' + text + '" id="' + value + '" />';
                $(c_select[j]).parent().html(newinput);
                $(c_select[j]).attr("disabled", "disabled");
            }
            else {
                var _name = $(c_select[j]).attr("name");
                var sel = $(tr_id).find('select[name=' + _name + ']');
                var value = $(sel).val();
                var newinput;
                if(value)
                {
                    newinput = '<input name="' + _name + '" class="' + $(sel).attr('class') + '" type="text"  disabled="true" value="' + value + '" id="' + value + '" />';

                }else
                {
                    newinput = '<input name="' + _name + '" class="' + $(sel).attr('class') + '" type="text"  disabled="true" value="" id="" />';
                }
                $(c_select[j]).parent().html(newinput);
                $(c_select[j]).attr("disabled", "disabled");
            }
        }
        var c_td = $(element).find("td");
        if (c_td.length) {
            _addButtonEditDelete(c_td[c_td.length - 1]);
        }

    };
    var _addEmpl = this.addEmpl = function addEmpl(tr_id,tbl_id) {

        switch (_list_type) {

            case 'staff':
            {
                if ($('#addName').val() != null) {
                    var tr_clone = $('#'+tr_id).clone();
                    var staff_id = $('#'+tr_id+' #addName option:selected').val();
                    $(tr_clone).attr("id", staff_id);
                    var obj = {};
                    obj['addName'] = staff_id;
                    var c_input = $(tr_clone).find("input");
                    for (var i = 1; i <= c_input.length; i++) {
                        var _name = $(c_input[i]).attr("name");
                        var test = $(tr_id).find('input[name=' + _name + ']');
                        var value = $(test).val();
                        obj[_name] = value;
                    }
                    _ajaxAddProjectStaff(obj);
                    //ajax insert here
                    $(tr_clone).attr("isout", $('#'+tr_id+' #addName option:selected').attr("isout"));
                    _setValueForClone(tr_clone,'#'+tr_id);
                    $('#'+tr_id+' #addName option:selected').remove();
                    $('#'+tr_id+' #addName').trigger("chosen:updated");
                    $('#'+tr_id+' #addName').trigger("change");
                    $('#'+tbl_id+' #'+tr_id).before(tr_clone);

                } else {
                    alert("Vui lòng chọn nhân sự");
                    return;
                };
                break;
            }
            case 'work': {
                if ($('#manager').val() || $('#member').val()) {
                    var tr_clone = $('#' + tr_id).clone();
                    var work_id = "asssgfsdgsdg";
                    $(tr_clone).attr("id", work_id);
                    //ajax insert here
                    _setValueForClone(tr_clone, '#' + tr_id);
                    $('#' + tr_id + ' #manager option:selected').remove();
                    $('#' + tr_id + ' #member option:selected').each(function () {
                        $('#' + tr_id + ' #manager option[value=' + $(this).val() + ']').remove();
                        $(this).remove();

                    });
                    //$('#'+tr_id+' #member option:selected').remove();
                    $('#' + tr_id + ' #manager').trigger("chosen:updated");
                    $('#' + tr_id + ' #manager').trigger("change");
                    $('#' + tr_id + ' #member').trigger("chosen:updated");
                    $('#' + tr_id + ' #member').trigger("change");
                    $('#' + tbl_id + ' tbody').prepend(tr_clone);
                    break;
                }else {
                    alert("Vui lòng chọn nhân sự");
                    return;
                };
            }
            case 'work_detail':
            {
                var tr_clone = $('#'+tr_id).clone();
                var work_id = "asssgfsdgsdg";
                $(tr_clone).attr("id", work_id);
                //ajax insert here
                _setValueForClone(tr_clone,'#'+tr_id);
                $('#'+tbl_id+' tbody').prepend(tr_clone);
                break;
            }

            default:
            {
                var tr_clone = $('#'+tr_id).clone();
                _setValueForClone(tr_clone,'#'+tr_id);
                $('#'+tbl_id+' tbody').prepend(tr_clone);
                break;
            }
        }

        $(".edit-emp").click(function (e) {
            e.preventDefault();
            if ($(this).attr('edit-event') === undefined) {
                _editEmpl(this);
                $(this).attr('edit-event', true);
            }

        });
        $(".delete-emp").click(function (e) {
            e.preventDefault();
            if ($(this).attr('delete-event') === undefined) {
                _deleteEmpl(this);
                $(this).attr('delete-event', true);
            }

        });



    };
    var _ajaxAddExEmpl = this.ajaxAddExEmpl = function ajaxAddExEmpl(array) {
        console.log(array);

        var id = '';
        // var token = $("meta[name='csrf-token']").attr("content");
        // data.push('_token', token);

        // data.append('_id', id);
        $.ajax({
            type : 'POST',
            dataType: "json",
            url  : '/erp/employer/createstaffout',
            data : array,
            async: false,
            success :  function(getdata)
            {
                if(getdata.success){
                    // console.log(getdata.id);
                    id = ( ( getdata.id.$oid===null  ) ? '' : getdata.id.$oid);

                    console.log( id);
                }
            }
        });

        return id;

    };
    var _ajaxDelete = this.ajaxDelete = function ajaxDelete(id) {
        var data = {};
        data['staff_id'] = id;
        $.ajax({
            type : 'POST',
            dataType: "json",
            url  : '/erp/project/staff/delete',
            data : data,
            async: false,
            success :  function(getdata)
            {

            }
        });
    };
    var _ajaxAddProjectStaff = this.ajaxAddExEmpl = function ajaxAddExEmpl(array) {

        console.log(array);

        var id = '';
        // var token = $("meta[name='csrf-token']").attr("content");
        // data.push('_token', token);

        // data.append('_id', id);
        $.ajax({
            type : 'POST',
            dataType: "json",
            url  : '/erp/project/staff/add',
            data : array,
            async: false,
            success :  function(getdata)
            {
                if(getdata.success){
                    alert('thêm thành công');
                }
                else{
                    var error = (getdata.error !==undefined) ? getdata.error : '';
                    alert(error);
                }
            }
        });

        return id;

    };
    var _ajaxEditProjectStaff = this.ajaxEditProjectStaff = function ajaxEditProjectStaff(array) {

        console.log(array);

        var id = '';
        // var token = $("meta[name='csrf-token']").attr("content");
        // data.push('_token', token);

        // data.append('_id', id);
        $.ajax({
            type : 'POST',
            dataType: "json",
            url  : '/erp/project/staff/update',
            data : array,
            async: false,
            success :  function(getdata)
            {
                if(getdata.success){
                    alert('ok');
                }
                else{
                    var error = (getdata.error !==undefined) ? getdata.error : '';
                    alert(error);
                }
            }
        });

        return id;

    };
    var _init = this.init = function (type) {
        if(type)
            _list_type=type;
        else
            _list_type="staff";
    };
    _init("staff");
    return this;
};

