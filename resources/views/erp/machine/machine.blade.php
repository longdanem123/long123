@extends('erp.layouts.app')

@section('content')
    <div class="content-page document">
        <div class="form-search">
            <div class="pull-left">

                <h3 class="title-page">Danh sách thiết bị</h3>
            </div>
            <div class="pull-right">
                <p class="btn-box btn-green">
                    <i class="fa fa-plus"></i>
                    <a href="#" data-toggle="modal" data-target="#createMachines" class="createMachines">Thêm</a>
                </p>
                <p class="btn-box btn-green">
                    <i class="fa fa-trash-o"></i>
                    <input class="btn-icon" type="button" value="Xóa" />
                </p>
            </div>
        </div>
        <table class="table table-striped nowrap tbl-web-search">
            <thead>
            <tr>
                <th>STT</th>
                <th>Tên thiết bị</th>
                <th>Ngày nhập</th>
                <th>Đơn giá</th>
                <th>ĐVT</th>
                <th>Số lượng</th>
                <th>Còn lại</th>
                <th>Ghi chú</th>
                <th>Xử lý</th>
            </tr>

            </thead>
            <tbody>
            <div class="clone-tr hide">
                <tr class="">
                    <td>1</td>
                    <td class="name">Tên thiết bị</td>
                    <td class="buy-date">Ngày nhập</td>
                    <td class="unit-price">Đơn giá</td>
                    <td class="unit">ĐVT</td>
                    <td class="quantity">Số lượng</td>
                    <td class="used">Còn lại</td>
                    <td class="note">Ghi chú</td>
                    <td>
                        <p class="btn-box btn-trans">
                            <i class="fa fa-pencil"></i>
                            <a href="#" data-toggle="modal" data-target="#editMachines"></a>
                        </p>
                        <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                    </td>
                </tr>
            </div>
            @foreach($machines as $rows)
            <tr data-id="{{$rows['_id']}}">
                <td>{{$loop->index+1}}</td>
                <td>{{ $rows['name'] or "" }}</td>
                <td>{{ $rows['buy_date'] or "" }}</td>
                <td>{{ $rows['unit_price'] or "" }}</td>
                <td>{{ $rows['unit'] or "" }}</td>
                <td>{{ $rows['quantity'] or "" }}</td>
                <td>{{ $rows['used'] or "" }}</td>
                <td>{{ $rows['note'] or "" }}</td>
                <td>
                    <p class="btn-box btn-trans">
                        <i class="fa fa-pencil"></i>
                        <a href="#" data-toggle="modal" data-target="#editMachines"></a>
                    </p>
                    <button type="button" class="deleteDoc btn-trans"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    <div class="modal fade" id="editMachines" tabindex="-1" role="dialog" aria-labelledby="myEditMachines">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-editMachines">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myEditMachines">Sửa thiết bị</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên thiết bị:</label>
                                <input type="text" class="form-control form-data" name="editNameMar" id="editNameMar" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày nhập:</label>
                                <input data-provide="datepicker form-data" class="form-control " name="" id=""  placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Đơn giá:</label>
                                <input type="text" class="form-control form-data" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>ĐVT:</label>
                                <input type="text" class="form-control form-data" name="" id="" placeholder="Nhập..."/>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số lượng</label>
                                <input type="text" class="form-control form-data" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Còn lại</label>
                                <input type="text" class="form-control form-data" name="" id="" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control form-data" rows="3" placeholder="Nhập..."></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="createMachines" tabindex="-1" role="dialog" aria-labelledby="myCreateMachines">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="post" role="form" id="form-createMachines" action="{{ route('erp.machine.create.post') }}">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myCreateMachines">Thêm thiết bị</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Tên thiết bị:</label>
                                <input type="text" class="form-control form-data" name="name" id="name" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Ngày nhập:</label>
                                <input data-provide="datepicker" class="form-control form-data" name="buy_date" id="buy_date"  placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Đơn giá:</label>
                                <input type="text" class="form-control form-data" name="unit_price" id="unit_price" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>ĐVT:</label>
                                <input type="text" class="form-control form-data" name="unit" id="unit" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Số lượng</label>
                                <input type="text" class="form-control form-data" name="quantity" id="quantity" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="form-group">
                                <label>Còn lại</label>
                                <input type="text" class="form-control form-data" name="used" id="used" placeholder="Nhập..." />
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="">
                                <label class="lbl-form">Mô tả:</label>
                                <textarea class="form-control form-data" rows="3" placeholder="Nhập..." name="note" id="note"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Lưu</button>
                </div>
            </form>
        </div>
    </div>
    <script src="{{ asset('js/machine.js') }}"></script>
    <script src="{{ asset('js/staff.js') }}"></script>
@endsection