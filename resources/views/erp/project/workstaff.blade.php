@extends('erp.layouts.app')

@section('content')
    <?php
    if (Session::has('idProject')) {
        $idProject = Session::get('idProject');
    }
    ?>
    <div class="group-breadcrumb">
        <ol class="breadcrumb pull-left">
            <li><a href="{{ route("erp") }}"><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <li id="menueml">Dự án</li>
            <li class="active">Bảng phân công</li>
        </ol>
    </div>
    <div class="content-page-project">
        @include('erp.layouts.leftmenu')
        <div class="tab-content project-right " >
            <div role="tabpanel" class="tab-pane active" id="work">
                <div class="header-work">
                    <div class="group-title">
                        <h3 class="title-page">Bảng phân công</h3>
                        <h5 class="des-page"></h5>
                    </div>
                    <div class="pull-right">
                        <div class="pull-left">
                            <p class="btn-box btn-green upload-file">
                                <i class="fa fa-upload"></i>
                                <input class="btn-icon choose-file" type="file" value="Upload"/>
                            </p>
                            <p class="btn-box btn-green">
                                <i class="fa fa-download"></i>
                                <input class="btn-icon" type="button" value="Download"
                                       onclick="location.href = '{{ url('erp/project/worklist/download/'.$idProject) }}';"/>
                            </p>
                        </div>
                        <p class="input-search">
                            <i class="fa fa-search"></i>
                            <input type="text" class="form-control" placeholder="Nhập thông tin tìm kiếm..."/>
                            <input class="btn-icon" type="submit" value=""/>
                        </p>
                        <ul class="nav nav-tabs tab-work">
                            <li><a href="{{ url('erp/project/work/'. $idProject) }}" id="link-view1"><i
                                            class="fa fa-th-large" aria-hidden="true"></i></a></li>
                            <li><a href="{{ url('erp/project/worklist/'. $idProject) }}" id="link-view2"><i
                                            class="fa fa-th-list" aria-hidden="true"></i></a></li>
                            <li><a href="{{ url('erp/project/workstaff/'. $idProject) }}" id="link-view3"><i
                                            class="fa fa-user"></i></a></li>
                        </ul>
                    </div>
                </div>

                <div class="tab-content">
                    <div id="view3" class="tab-pane fade in active">
                        <ul class="accordion">
                            @foreach($data as $task_staff)
                                <li class="item-accordion">
                                    <a class="title-accordion">{{ $task_staff['staff_name'] }}<span
                                                class="fa fa-minus"></span></a>
                                    <div class="content-accordion">
                                        @if(!empty($task_staff['task_staff']))
                                            <table class="table table-striped nowrap tbl-work" cellspacing="0"
                                                   width="100%">
                                                <thead>
                                                <tr>
                                                    <th rowspan="2"></th>
                                                    <th rowspan="2">Tên công việc</th>
                                                    <th colspan="2">Thời gian</th>
                                                    <th rowspan="2">ĐVT</th>
                                                    <th rowspan="2">Tổng khối lượng</th>
                                                    <th rowspan="2">Tiến trình</th>
                                                    <th rowspan="2"></th>
                                                </tr>
                                                <tr>
                                                    <th>Bắt đầu</th>
                                                    <th>Kết thúc</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($task_staff['task_staff'] as $task)
                                                    <tr class="add-data">
                                                        <td>
                                                            <input type="checkbox" class="cbx-select"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="" value="{{ $task['name'] }}"/>
                                                        </td>
                                                        <td>
                                                            <input data-provide="datepicker" name="start" id="start"
                                                                   class="" placeholder="{{ $task['start'] }}"/>
                                                        </td>
                                                        <td>
                                                            <input data-provide="datepicker" name="end" id="end"
                                                                   class=""
                                                                   placeholder="{{ $task['end'] }}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="" value="{{ $task['unit'] }}"/>
                                                        </td>
                                                        <td>
                                                            <input type="text" class="" value="{{ $task['total'] }}"/>
                                                        </td>
                                                        <td class="process">
                                                            <div class="progress-group">
                                                        <span class="progress-text">
                                                            <span class="start">{{ $task['start'] }}</span> <span
                                                                    class="end">{{ $task['end'] }}</span>
                                                        </span>
                                                                <span class="progress-number">{{ $task['total'] }}
                                                                    %</span>

                                                                <div class="progress sm">
                                                                    <div class="progress-bar progress-bar-aqua"
                                                                         style="width: 80%"></div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button type="submit" class="add-emp"><i
                                                                        class="fa fa-plus-circle"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            Chưa có công việc
                                        @endif
                                    </div>
                                </li>
                                <br><br><br><br>
                            @endforeach
                            <li class="item-accordion">
                                <a class="title-accordion">Công việc chưa phụ trách<span
                                            class="fa fa-minus"></span></a>
                                <div class="content-accordion">
                                    @if(!empty($workqueue))
                                        <table class="table table-striped nowrap tbl-work" cellspacing="0"
                                               width="100%">
                                            <thead>
                                            <tr>
                                                <th rowspan="2"></th>
                                                <th rowspan="2">Tên công việc</th>
                                                <th colspan="2">Thời gian</th>
                                                <th rowspan="2">ĐVT</th>
                                                <th rowspan="2">Tổng khối lượng</th>
                                                <th rowspan="2">Tiến trình</th>
                                                <th rowspan="2"></th>
                                            </tr>
                                            <tr>
                                                <th>Bắt đầu</th>
                                                <th>Kết thúc</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($workqueue as $task)
                                                <tr class="add-data">
                                                    <td>
                                                        <input type="checkbox" class="cbx-select"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="" value="{{ $task['name'] }}"/>
                                                    </td>
                                                    <td>
                                                        <input data-provide="datepicker" name="start" id="start"
                                                               class="" placeholder="{{ $task['start'] }}"/>
                                                    </td>
                                                    <td>
                                                        <input data-provide="datepicker" name="end" id="end"
                                                               placeholder="{{ $task['end'] }}"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="" value="{{ $task['unit'] }}"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" class="" value="{{ $task['total'] }}"/>
                                                    </td>
                                                    <td class="process">
                                                        <div class="progress-group">
                                                        <span class="progress-text">
                                                            <span class="start">{{ $task['start'] }}</span> <span
                                                                    class="end">{{ $task['end'] }}</span>
                                                        </span>
                                                            <span class="progress-number">{{ $task['total'] }}
                                                                %</span>

                                                            <div class="progress sm">
                                                                <div class="progress-bar progress-bar-aqua"
                                                                     style="width: 80%"></div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button type="submit" class="add-emp"><i
                                                                    class="fa fa-plus-circle"></i></button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        Chưa có công việc
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--////////////////////////////////////////////////////////// -->
    <script src="{{ asset('js/staff.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".upload-file .choose-file").change(function () {
                var fileExtension = ['xls', 'xlsx'];
                var empGroup = $(".group-employee");
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Only formats are allowed : " + fileExtension.join(', '));
                    return;
                }
                var data = new FormData();
                data.append('file', $('.upload-file .choose-file').prop('files')[0]);
                $.ajax({
                    type: 'POST',
                    dataType: "json",
                    url: '/erp/project/workstaff/upload',
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        alert('uploaded !');
                    }
                });
                return false;
            });
        });
    </script>
@endsection