<?php

namespace App\ERPModels;

use App\Helpers\Helper;
use DB;
use Jenssegers\Mongodb\Eloquent\Model;

use Session;

class Project extends Model
{
	//
	protected $collection = 'projects';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'project_staff',
		'project_machine',
		'materials_id',
		'department_id',
		'name',
		'start',
		'end',
		'price',
		'description',
		'group_name',
		'group_task',
		'working_times',
		'address',
		'income',
		'project_templates_id',
		'status',
	];

	public function __construct()
	{
		$dbname = Session::get('dbname');
		$this->connection = $dbname;
	}

	/**
	 * @Author: PhuTran
	 * @param: $data[]
	 * @description: Add project
	 */
	public function __createProject($data = [])
	{
		$data['status'] = isset($data['status']) ? $data['status'] : 0;
		if (isset($data['staff_id'])) {
			$arrStaff = array();
			$arrStaff[] = array(
				'id'     => $data['staff_id'],
				'in'     => '',
				'out'    => '',
				'action' => 0,
			);
			$data['project_staff'] = $arrStaff;
		}
		foreach ($data as $key => $value) {
			if (in_array($key, $this->fillable)) {
				$arr[$key] = $value;
			}
		}

		return DB::connection($data['dbname'])->collection('projects')->insert($arr);
	}

	/**
	 * @Author PhuTran
	 * @description move project to trash
	 * @param array $data
	 * @return mixed
	 */
	public function __moveToTrash($data = [])
	{
		$id = isset($data['_id']) ? $data['_id'] : '';

		return DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(['status' => 0]);
	}

	/**
	 * @Author PhuTran
	 * @description Update project
	 * @param array $data
	 * @return true || false
	 */
	public function __update($data = [])
	{
		$id = isset($data['_id']) ? $data['_id'] : '';
		$findByIdProject = $this::find($id);
		if (!$findByIdProject) {
			return [];
		} else {
			if (isset($data['name'])) {
				$nameProject = $data['name'];
				$findByIdProject->name = $nameProject;
			}
			if (isset($data['start'])) {
				$startDate = $data['start'];
				$findByIdProject->start = $startDate;
			}
			if (isset($data['end'])) {
				$endDate = $data['end'];
				$findByIdProject->end = $endDate;
			}
			if (isset($data['description'])) {
				$description = $data['description'];
				$findByIdProject->description = $description;
			}
			$temp = array();
			if (isset($data['staff_id'])) {  // update manager project
				$temp = $findByIdProject->project_staff;
				$temp[0]['id'] = $data['staff_id'];
				$findByIdProject['project_staff'] = $temp;
			}

			return $findByIdProject->save();
		}
	}

	/**
	 * @author PhuTran
	 * @description Get data project by idProject
	 * @param array $data
	 * @return array
	 */
	public function __getById($data = [])
	{
		$id = isset($data['_id']) ? $data['_id'] : '';
		$findByIdProject = $this::find($id);
		if (!$findByIdProject) {
			return [];
		} else {
			return $findByIdProject;
		}
	}

	public function getNameById($id)
	{
		return DB::connection(Session::get('dbname'))->collection('projects')->where('_id', $id)->pluck('name')->first();
	}


    /**
     * @author Long
     * @description danh sách nhân viên trong dự án
     * @param array $data
     * @return array
     */
    public function __getProjectStaff($data = [])
    {
        $id = isset($data['_id']) ? $data['_id'] : '';
        return DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->pluck('project_staff');
    }

    /**
     * @author Long
     * @description them nhan su vao du an
     * @param array $data
     * @return array
     */
    public function __addStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';

        $project_staff = [];
        $findByIdProject = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->first();
        if (!$findByIdProject) {

            return [];
        } else {
            if (isset($data['addName'])) {

                $project_staff['id'] = isset($data['addName']) ? $data['addName'] : '';
                $project_staff['in'] = isset($data['addStartDate']) ? $data['addStartDate'] : '';
                $project_staff['out'] = isset($data['addEndDate']) ? $data['addEndDate'] : '';
                $project_staff['action'] = 1;
                $project_staff['res'] = isset($data['addJob']) ? $data['addJob'] : '';
                $project_staff['note'] = isset($data['addDes']) ? $data['addDes'] : '';

            }

        }
        DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->push('project_staff', $project_staff);
    }

    /**
     * @author Long
     * @description xóa nhân sự dự án
     * @param array $data
     * @return array
     */
    public function __deleteStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';
        $staff_id = isset($data['staff_id']) ? $data['staff_id'] : '';
//        $project_staff = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->find('project_staff.');
//        dd($project_staff);
        return DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->pull('project_staff', ['id' => $staff_id]);

    }

    /**
     * @author Long
     * @description update nhân sự dự án
     * @param array $data
     * @return array
     */
    public function __updateDataStaff($data = [])
    {   $id = isset($data['_id']) ? $data['_id'] : '';

        $project_staff = [];
        $findByIdProject = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->first();
        if (!$findByIdProject) {

            return [];
        } else {
            $project_staff['id'] = isset($data['addName']) ? $data['addName'] : '';
            $project_staff['in'] = isset($data['addStartDate']) ? $data['addStartDate'] : '';
            $project_staff['out'] = isset($data['addEndDate']) ? $data['addEndDate'] : '';
            $project_staff['action'] = 1;
            $project_staff['res'] = isset($data['addJob']) ? $data['addJob'] : '';
            $project_staff['note'] = isset($data['addDes']) ? $data['addDes'] : '';
            $array_staff = DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->pluck('project_staff')->toArray();


            $index = '';
            $count = 0;
            foreach ($array_staff[0] as $key => $value){

                $count++;
                if($value['id']==$project_staff['id']){
                    $index = $key;
                }
            }
            $project_staff['action'] = $index==0 ? 0 : 1;

            DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(array('$set' => array('project_staff.'.$index => $project_staff)));
        }

//            ->update([['project_staff.id' => $project_staff['id']],'set' => ['project_staff.note' => '1111']]);
//        DB::connection($data['dbname'])->collection('projects')->where('_id', $id)->update(array('project_staff.id' => $project_staff['id']), array('project_staff.note' => '327483274897'));
    }

	public function getStaffList($id)
	{
		return DB::connection(Session::get('dbname'))->collection('projects')->where('_id', $id)->pluck('project_staff');
	}

    /*
     * @Author: Bao Long
     * @Description: danh sach project
     * @var array data
     */
    public function __getList($data = []){
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        if(!empty($keyword)){

            return DB::connection($data['dbname'])->collection('projects')->where($arr)->where('name', 'LIKE', "%$keyword%")->get();
        }
        return DB::connection($data['dbname'])->collection('projects')->where($arr)->get();
    }
}
