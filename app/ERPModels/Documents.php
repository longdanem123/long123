<?php

namespace App\ERPModels;

use Jenssegers\Mongodb\Eloquent\Model as Model;
use Session;
use Illuminate\Support\Facades\DB;

class Documents extends Model
{
    //
	protected $collection = 'documents';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'name',
		'project_id',
		'file',
		'date',
	    'to',
	    'from',
	    'group_name',
	    'departments',
	    'status',
        'desc',
        'note',
        'status'
	];

    /*
     * @Author: Bao Long
     * @Description: danh sach document
     * @var array data
     */
    public function __getList($data = []){
        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        if(!empty($keyword)){

            return DB::connection($data['dbname'])->collection('documents')->where($arr)->where('name', 'LIKE', "%$keyword%")->orderBy('_id','desc')->get();
        }
        return DB::connection($data['dbname'])->collection('documents')->where($arr)->orderBy('_id','desc')->get();
    }

    /*
     * @Author: Bao Long
     * @Description: them 1 document
     * @var array data
     */
    public function __create($data = []){
        $data['status'] = isset($data['status']) ? $data['status'] : 0;
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        return DB::connection($data['dbname'])->collection('documents')->insertGetId($arr);
    }

    /*
     * @Author: Bao Long
     * @Description: update 1 document
     * @var array data
     */
    public function __update($data = []){
        $id = isset($data['_id']) ? $data['_id'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }

        return DB::connection($data['dbname'])->collection('documents')->where('_id',$id)->update($arr);

    }

    /*
     * @Author: Bao Long
     * @Description: lay 1 document
     * @var array data
     */
    public function getDocument($data =[]){
        $id = isset($data['_id']) ? $data['_id'] : '';
        $arr = [];
        foreach ($data as $key => $value)
        {
            if(in_array($key,$this->fillable)){
                $arr[$key] = $value;
            }
        }
        if(!empty($id)){
            return DB::connection($data['dbname'])->collection('documents')->where($arr)->where('_id','<>',$id)->first();
        }
        return DB::connection($data['dbname'])->collection('documents')->where($arr)->first();
    }
}
