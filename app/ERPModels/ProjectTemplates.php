<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectTemplates extends Model
{
    //
	protected $collection = 'project_templates';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'name',
		'template_structure',
		'status',
	];
}
