<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    //
	protected $collection = 'departments';
	protected $connection = 'mongodb';

	protected $fillable = [
		'code',
		'name',
		'status',
	];
}
