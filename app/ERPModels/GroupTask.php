<?php

namespace App\ERPModels;

use App\Helpers\Helper;
use DB;
use Jenssegers\Mongodb\Eloquent\Model;

use Session;

class GroupTask extends Model
{
	//
	protected $collection = 'group_task';
	protected $connection = 'mongodb';

	protected $fillable = [
		'project_id',
		'name',
		'alias',
		'status',
	];

	public function __construct()
	{
		$dbname = Session::get('dbname');
		$this->connection = $dbname;
	}

	/**
	 * @author PhuTran
	 * @description create data group task
	 * @param array $data
	 * @return mixed
	 */
	public function __createGroupTask($data = [])
	{
		$idProject = isset($data['project_id']) ? $data['project_id'] : '';
		if(empty($idProject)) {
			return [];
		} else {
			$this->project_id = $idProject;
			$this->name = $data['name'];
			$this->alias = Helper::seoURL($data['name']);
			$this->status = 1;
			return $this->save();
		}
	}

	public function __getGroupTaskByProjectId($data = [])
	{
		$dbName = Session::get('dbname');
		$id = isset($data['project_id']) ? $data['project_id'] : '';
		$find = DB::connection($dbName)->collection('group_task')->where('project_id', $id)->first();
		if(!$find) {
			return [];
		} else {
			return $find;
		}
	}

	public function __updateGroupTask($data = [])
	{
		$idGroupTask = isset($data['_id']) ? $data['_id'] : '';
		$find = $this::find($idGroupTask);
		if(!$find) {
			return [];
		} else {
			$find->name = $data['name'];
			$find->alias = Helper::seoURL($data['name']);
		}

		return $find->save();
	}

	/**
	 * @author PhuTran
	 * @description delete group task
	 * @param array $data
	 * @return array
	 */
	public function __deleteGroupTask($data = [])
	{
		$idGroupTask = isset($data['_id']) ? $data['_id'] : '';
		$find = $this::find($idGroupTask);
		if(!$find) {
			return [];
		} else {
			return $this::where('_id', $idGroupTask)->delete();
		}
	}
}
