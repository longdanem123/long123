<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Model;

class Post extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'content',
        'category'
    ];

    public function listDB() {
        
    }

}
