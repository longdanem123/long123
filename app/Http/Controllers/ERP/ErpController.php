<?php

namespace App\Http\Controllers\ERP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Config;
use Illuminate\Support\Facades\DB;

class ErpController extends Controller
{
    public function __construct() {
        $this->middleware('logged');
    }
    public function index()
    {
        return view('erp.dashboard');
    }
     
     public function eml()
    {
        $dbname = Session::get('dbname');
        dd(DB::connection($dbname)->collection('test')->get());
    }
    public function getBlank()
    {
        return view('erp.blank');
    }
}
