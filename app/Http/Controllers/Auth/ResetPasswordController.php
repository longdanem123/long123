<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Models\SystemUser;
class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */


    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('guest');
    }

    public function changePass(Request $request)
    {
        $password = DB::collection('admins')->where('_id', Session::get('user')['_id'])->pluck('password')->first();
        if ($request->pass == $password) {
            DB::collection('admins')->where('_id', Session::get('user')['_id'])
                ->update(array("password" => $request->newpass));
            echo 'Đã đổi mật khẩu';
        } else {
            echo 'Sai mật khẩu';
        }
    }
}
