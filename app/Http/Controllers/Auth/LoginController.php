<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use app\Models\User;
use Illuminate\Support\Facades\DB;
use Hash;
use Session;
use Config;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      //  $this->middleware('logged');
    }

    public function showLoginForm() { 
        return view('auth.login');
    }
    
    public function login(Request $request) {
        $password = DB::collection('admins')->where('username', $request->username)->pluck('password')->first();

        if ($request->password == $password) {
            $user = DB::collection('admins')->where('username', $request->username)->first();
            Session::put('user', $user);

            if(isset($user['dbname']))
                Session::put('dbname', $user['dbname']);
            else
                Session::put('dbname', 'N/A');
            return Redirect::route('home');
        } else {
            return redirect()->back()
                ->with('message', 'Failed!')
                ->with('status', 'danger')
                ->withInput();
        }
    }

    protected function changeEnv($data = array()){
        if(count($data) > 0){
            $env = file_get_contents(base_path() . '/.env');
            $env = preg_split('/\s+/', $env);;
            foreach((array)$data as $key => $value){
                foreach($env as $env_key => $env_value){
                    $entry = explode("=", $env_value, 2);
                    if($entry[0] == $key){
                        $env[$env_key] = $key . "=" . $value;
                    } else {
                        $env[$env_key] = $env_value;
                    }
                }
            }
            $env = implode("\n", $env);
            file_put_contents(base_path() . '/.env', $env);
            
            return true;
        } else {
            return false;
        }
    }
}
